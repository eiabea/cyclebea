# Cyclebea

[![pipeline status](https://gitlab.com/eiabea/cyclebea/badges/master/pipeline.svg)](https://gitlab.com/eiabea/cyclebea/-/commits/master)
[![coverage report](https://gitlab.com/eiabea/cyclebea/badges/master/coverage.svg)](https://gitlab.com/eiabea/cyclebea/-/commits/master)

# Description

Cyclebea is a simple [nestjs](https://nestjs.com/) web application which writes [kmz](https://de.wikipedia.org/wiki/Keyhole_Markup_Language) files provided by [OpenTracks](https://github.com/OpenTracksApp/OpenTracks) to an [InfluxDB](https://www.influxdata.com/products/influxdb-overview/) and displays the data with [Grafana](https://grafana.com/)

# Motivation

[OpenTracks](https://github.com/OpenTracksApp/OpenTracks) is a great open-source android app for tracking various sport activities, but it lacks an overview feature to display data like `Total distance` or `Average speed`. This web application aims to provide this missing feature.

# Privacy

This project is intend to be deployed on a private server to keep all the data in the hand of the user.

# Installation

```bash
$ npm install
```

# Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

# Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# Deployment

## Requirements

- [Docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [traefik](https://docs.traefik.io/)

## Setup

#### Download `docker-compose-prod.yml` to your server

```bash
$ curl https://gitlab.com/eiabea/cyclebea/-/raw/master/docker-compose-prod.yml > docker-compose.yml
```

#### Find out `traefik` network

```bash
$ docker inspect TRAEFIK_CONTAINER
```

```json
"Networks": {
    "web": { // <-- Name of the traefik network
        "IPAMConfig": null,
        "Links": null,
        "Aliases": [
            "traefik",
            "b36ca89ce9f2"
        ],
    }
}
```

#### Use your favorite text editor to change these values according to your setup

Set the correct docker network from the previous step on all labels and in the network section

#### Protect endpoints with traefik's basic auth middleware

```yaml
  cyclebea:
    image: registry.gitlab.com/eiabea/cyclebea:latest
    labels:
      - "traefik.http.routers.cyclebea-api.middlewares=test-auth@docker"
      - "traefik.http.middlewares.test-auth.basicauth.users=foo:$$2y$$10$$CtbslbUd9cttcY60qI8PVut4fRekWrI3XK9EdllQENHbBIHGIEyUC"
```

Please note that all `$` have to be duplicated to get escaped as described [here](https://docs.traefik.io/middlewares/basicauth/#configuration-examples)!

I used [this](https://hostingcanada.org/htpasswd-generator/) generator for creating the bcrypted password

## Grafana

#### Default credentials:

- Username: admin
- Password: admin

#### Add data source

1. Click on `Add data source`
2. Choose `InfluxDB`
3. Set `URL` to `http://influx:8086`
4. Set `Database` to `cyclebea`
5. Click on `Save & Test`

#### Import dashboard

1. Copy the latest dashboard from [GitLab](https://gitlab.com/eiabea/cyclebea/-/tree/master/grafana)
2. Hover over the `+` on the right hand side
3. Click on `Import`
4. Paste the contents of the copied dashboard into the text area below `Or paste JSON`
5. Click on `Load`
6. Click on `Import`