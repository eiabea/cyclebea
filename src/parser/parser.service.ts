import { Injectable, Logger } from '@nestjs/common';
import * as tj from '@tmcw/togeojson';
import * as moment from 'moment'
import { DOMParser } from 'xmldom';
import { Trip } from 'src/model/trip';

@Injectable()
export class ParserService {
  private readonly logger = new Logger(ParserService.name);

  private normalizeDuration(dur: string): string {
    let result: string;
    const split = dur.split(':');
    if (split.length === 2) {
      result = `00:${dur}`;
    } else {
      result = dur;
    }
    return result;
  }

  public parseFiles(files: any[]): Trip[] {
    this.logger.verbose(`Parsing ${files.length} files`);
    return files.map(file => {
      const kml = new DOMParser().parseFromString(file);

      const converted = tj.kml(kml);

      // Narrow array down to only properties
      const props = converted.features.map(feature => feature.properties);

      // Looking for property with #end
      const endArr = props.filter(prop => {
        return prop.styleUrl === '#end';
      })

      // Splitting up description string into an arrqay
      const descArr = endArr[0].description.split('\n')

      let movingTime: number;
      let totalTime: number;
      let totalDistance = 0;
      let maxSpeed = 0;
      let averageMovingSpeed = 0;
      let maxElevation = 0;
      let minElevation = 0;
      let elevationGain = 0;
      let recorded = 0;

      for (const entry of descArr) {
        let regexResult = [];
        if (/Total distance: (\d*.\d*) km/g.test(entry)) {
          regexResult = /Total distance: (\d*.\d*) km/g.exec(entry);
          totalDistance = parseFloat(regexResult[1])
        }
        if (/Total time: (\d*:\d*:?\d*)/g.test(entry)) {
          regexResult = /Total time: (\d*:\d*:?\d*)/g.exec(entry);
          totalTime = moment.duration(this.normalizeDuration(regexResult[1])).asSeconds()
        }
        if (/Moving time: (\d*:\d*:?\d*)/g.test(entry)) {
          regexResult = /Moving time: (\d*:\d*:?\d*)/g.exec(entry);
          movingTime = moment.duration(this.normalizeDuration(regexResult[1])).asSeconds()
        }
        if (/Average moving speed: (\d*.\d*) km\/h/g.test(entry)) {
          regexResult = /Average moving speed: (\d*.\d*) km\/h/g.exec(entry);
          averageMovingSpeed = parseFloat(regexResult[1])
        }
        if (/Max speed: (\d*.\d*) km\/h/g.test(entry)) {
          regexResult = /Max speed: (\d*.\d*) km\/h/g.exec(entry);
          maxSpeed = parseFloat(regexResult[1])
        }
        if (/Max elevation: (\d*) m/g.test(entry)) {
          regexResult = /Max elevation: (\d*) m/g.exec(entry);
          maxElevation = parseFloat(regexResult[1])
        }
        if (/Min elevation: (\d*) m/g.test(entry)) {
          regexResult = /Min elevation: (\d*) m/g.exec(entry);
          minElevation = parseFloat(regexResult[1])
        }
        if (/Elevation gain: (\d*) m/g.test(entry)) {
          regexResult = /Elevation gain: (\d*) m/g.exec(entry);
          elevationGain = parseFloat(regexResult[1])
        }
        if (/Name: /g.test(entry)) {
          regexResult = /Name: (.*)/g.exec(entry);
          recorded = moment(regexResult[1], 'YYYY-MM-DD HH:mm').utc().unix();
        }
      }

      return {
        movingTime,
        totalTime,
        totalDistance,
        maxSpeed,
        averageMovingSpeed,
        maxElevation,
        minElevation,
        elevationGain,
        recorded,
      } as Trip
    })
  }
}
