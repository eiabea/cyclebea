export class UploadKmzDto {
  fieldname: string;
  originalname: number;
  encoding: string;
  mimetype: string;
  buffer: Buffer;
  size: number;
}