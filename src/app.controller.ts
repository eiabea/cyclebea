import { Controller, Get, Logger } from '@nestjs/common';
import { FileService } from './file/file.service';
import { ParserService } from './parser/parser.service';
import { InfluxService } from './influx/influx.service';
import { Trip } from './model/trip';

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  constructor(
    private readonly fileService: FileService,
    private readonly parserService: ParserService,
    private readonly influxService: InfluxService
  ) { }

  @Get('/reload')
  async getReload(): Promise<{ success: boolean, message: string }> {
    this.logger.verbose(`Reload triggered`);
    const files: string[] = await this.fileService.readFiles();
    this.logger.debug(`Read ${files.length} files`);

    const trips: Trip[] = await this.parserService.parseFiles(files);
    this.logger.debug(`Parsed ${trips.length} trips`);

    await this.influxService.writeTrips(trips);
    this.logger.log(`Successfully imported data`);

    return {
      success: true,
      message: `Successfully imported ${trips.length} trips into the database`
    }
  }
}
