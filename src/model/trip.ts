export class Trip {
  public movingTime: number;
  public totalTime: number;
  public totalDistance: number;
  public maxSpeed: number;
  public averageMovingSpeed: number;
  public maxElevation: number;
  public minElevation: number;
  public elevationGain: number;
  public recorded: number;
}
