import { Controller, HttpException, HttpStatus, Logger, Post, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { UploadKmzDto } from '../dto/UploadKmzDto';
import { FileService } from '../file/file.service';
import { InfluxService } from '../influx/influx.service';
import { Trip } from '../model/trip';
import { ParserService } from '../parser/parser.service';

@Controller('upload')
export class UploadController {
  private readonly logger = new Logger(UploadController.name);

  constructor(
    private readonly fileService: FileService,
    private readonly parserService: ParserService,
    private readonly influxService: InfluxService
  ) { }

  private async handleUpload(kmzArray: Buffer[]): Promise<void> {
    let decompressedKmzArray: string[];
    let tripArray: Trip[];

    try {
      decompressedKmzArray = await this.fileService.decompressKmz(kmzArray);
    } catch (err) {
      this.logger.error('Decompression failed', err);
      throw new HttpException('Unable to decompress uploaded file, is it a valid kmz file?', HttpStatus.BAD_REQUEST)
    }

    try {
      tripArray = this.parserService.parseFiles(decompressedKmzArray);
    } catch (err) {
      this.logger.error('Parsing failed', err);
      throw new HttpException('Unable to parse file, is it an open tracks file?', HttpStatus.BAD_REQUEST)
    }

    try {
      await this.influxService.writeTrips(tripArray);
    } catch (err) {
      this.logger.error('Writing trips failed', err);
      throw new HttpException('Unable write trips into database', HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Post('/')
  @UseInterceptors(FileInterceptor('kmz'))
  public async postUpload(@UploadedFile() kmz: UploadKmzDto): Promise<{ success: boolean, message: string }> {

    await this.handleUpload([kmz.buffer])

    return {
      success: true,
      message: `Successfully wrote uploaded kmz file to database`
    }
  }

  @Post('/bulk')
  @UseInterceptors(FilesInterceptor('kmz'))
  public async postBulkUpload(@UploadedFiles() kmz: UploadKmzDto[]): Promise<{ success: boolean, message: string }> {

    this.logger.debug(`Received bulk upload request with ${kmz.length} files`);

    await this.handleUpload(kmz.map(entry => entry.buffer))

    return {
      success: true,
      message: `Successfully wrote ${kmz.length} uploaded kmz files to database`
    }
  }
}
