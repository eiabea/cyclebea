import { Test, TestingModule } from '@nestjs/testing';
import { UploadController } from './upload.controller';
import { FileService } from '../file/file.service';
import { ConfigModule } from '@nestjs/config';
import { ParserService } from '../parser/parser.service';
import { InfluxService } from '../influx/influx.service';

describe('Upload Controller', () => {
  let controller: UploadController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UploadController],
      providers: [FileService, ParserService, InfluxService],
      imports: [ConfigModule],
    }).compile();

    controller = module.get<UploadController>(UploadController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
