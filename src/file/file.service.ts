import { Injectable, Module, Logger } from '@nestjs/common';
import * as fs from 'fs-extra';
import { join } from 'path';
import * as JSZip from 'jszip'
import { ConfigModule, ConfigService } from '@nestjs/config';

@Injectable()
@Module({
  imports: [ConfigModule],
})
export class FileService {
  private readonly logger = new Logger(FileService.name);

  constructor(private configService: ConfigService) { }
  /**
   * Reads all kmz files, un-compresses it and returns the content as string
   */
  public async readFiles(): Promise<string[]> {
    const KMZ_LOCATION = this.configService.get<string>('FILE_KMZ_LOCATION', '/tmp');

    this.logger.debug(`Reading files from ${KMZ_LOCATION}`);
    const allFiles: string[] = fs.readdirSync(KMZ_LOCATION);
    this.logger.debug(`Read ${allFiles.length} files from ${KMZ_LOCATION}`);
    const kmzFiles: string[] = allFiles.filter(file => /.*.kmz/g.test(file));
    this.logger.debug(`Filtered ${kmzFiles.length} kmz files from ${KMZ_LOCATION}`);

    this.logger.debug(`Creating buffer array from kmz files`);
    const bufferArray: Buffer[] = kmzFiles.map(kmzFile => {
      const kmzFilePath = join(KMZ_LOCATION, kmzFile);
      return fs.readFileSync(kmzFilePath)
    });

    return this.decompressKmz(bufferArray);
  }

  public async decompressKmz(compressedArray: Buffer[]): Promise<string[]> {
    this.logger.debug(`Decompressing ${compressedArray.length} kmz files`);
    return Promise.all(compressedArray.map(async compressed => {
      const uncompressed: JSZip = await JSZip.loadAsync(compressed);
      return uncompressed.file('doc.kml').async('string');
    }));
  }
}
