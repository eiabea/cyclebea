import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InfluxDB, FieldType } from 'influx'
import { Trip } from 'src/model/trip';

@Injectable()
export class InfluxService {
  private readonly logger = new Logger(InfluxService.name);

  private INFLUX_HOST: string = this.configService.get<string>('INFLUX_HOST', 'localhost');
  private INFLUX_PORT: string = this.configService.get<string>('INFLUX_PORT', '8086');
  private INFLUX_DB: string = this.configService.get<string>('INFLUX_DB', 'cyclebea');
  private INFLUX_MEASUREMENT: string = this.configService.get<string>('INFLUX_MEASUREMENT', 'trips');

  private client: InfluxDB;
  constructor(private configService: ConfigService) { }

  async onModuleInit() {
    this.logger.debug(`Initializing influx client`);
    this.client = new InfluxDB({
      host: this.INFLUX_HOST,
      port: parseInt(this.INFLUX_PORT, 10),
      database: this.INFLUX_DB,
      schema: [
        {
          measurement: this.INFLUX_MEASUREMENT,
          fields: {
            movingTime: FieldType.FLOAT,
            totalTime: FieldType.FLOAT,
            totalDistance: FieldType.FLOAT,
            maxSpeed: FieldType.FLOAT,
            averageMovingSpeed: FieldType.FLOAT,
            maxElevation: FieldType.FLOAT,
            minElevation: FieldType.FLOAT,
            elevationGain: FieldType.FLOAT,
          },
          tags: [],
        }
      ]
    });

    const databases = await this.client.getDatabaseNames();
    if (!databases.includes(this.INFLUX_DB)) {
      this.logger.warn(`Database "${this.INFLUX_DB}" not found, creating it`);
      await this.client.createDatabase(this.INFLUX_DB)
      this.logger.log(`Successfully created database "${this.INFLUX_DB}"`);
    }
    this.logger.log(`Successfully initialized influx client`);
  }

  public async writeTrips(trips: Trip[]): Promise<void> {
    const pointsToWrite = trips.map(trip => {
      return {
        tags: {},
        fields: {
          movingTime: trip.movingTime,
          totalTime: trip.totalTime,
          totalDistance: trip.totalDistance,
          maxSpeed: trip.maxSpeed,
          averageMovingSpeed: trip.averageMovingSpeed,
          maxElevation: trip.maxElevation,
          minElevation: trip.minElevation,
          elevationGain: trip.elevationGain,
        },
        // Influx saves time with ridiculous precision
        timestamp: trip.recorded * 1000 * 1000 * 1000
      }
    })

    await this.client.writeMeasurement(this.INFLUX_MEASUREMENT, pointsToWrite);

    this.logger.verbose(`Wrote ${pointsToWrite.length} trips to influx`);
  }
}
