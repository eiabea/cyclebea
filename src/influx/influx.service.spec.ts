import { Test, TestingModule } from '@nestjs/testing';
import { InfluxService } from './influx.service';
import { ConfigModule } from '@nestjs/config';

describe('InfluxService', () => {
  let service: InfluxService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InfluxService],
      imports: [ConfigModule],
    }).compile();

    service = module.get<InfluxService>(InfluxService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
