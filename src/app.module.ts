import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ParserService } from './parser/parser.service';
import { FileService } from './file/file.service';
import { ConfigModule } from '@nestjs/config';
import { InfluxService } from './influx/influx.service';
import { UploadController } from './upload/upload.controller';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [AppController, UploadController],
  providers: [ParserService, FileService, InfluxService],
})
export class AppModule { }
