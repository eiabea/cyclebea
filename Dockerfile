FROM node:12.16.1

COPY ./bin/wait-for-it.sh /usr/local/bin/wait-for-it.sh
RUN chmod +x /usr/local/bin/wait-for-it.sh

WORKDIR /app

EXPOSE 3000

CMD [ "npm", "run", "start:dev" ]
